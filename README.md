# Cluster Wiki 

Welcome to SDMC maintained wiki collecting Cluster knowledge!

This repository hosts a wiki, which serves as a centralized hub for all information related to our cluster system. The wiki is intended to collect guides, tutorials, troubleshooting resources, and community-contributed content to support and facilitate the use of the cluster.

**Accessing the Wiki:**

You can find the wiki linked in the sidebar on the left side of this GitLab repository. Simply click on "Wiki" to access it.

Click [here](https://gitlab.pik-potsdam.de/sdmc/cluster-wiki/-/wikis/home) to access the wiki directly.
